/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package praktikum2_gui1;

/**
 *
 * @author SAMSUNG
 */
public class MasyarakatSekitar extends Penduduk {
    private String nomor;

    MasyarakatSekitar() {
    }

    public MasyarakatSekitar(String dataNomor, String dataNama, String dataTempatTanggalLahir) {
        super(dataNama,dataTempatTanggalLahir);
        nomor = dataNomor;
    }
    
    public void setNomor(String dataNomor){
        this.nomor = nomor;
    }
    public String getNomor(){
        return nomor;
    }
    
     @Override
    public double hitungIuran() {
        double value = Double.parseDouble(nomor);
        double iuran = value*100;
        return iuran;
    }


}


