/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package praktikum2_gui1;

/**
 *
 * @author SAMSUNG
 */
public class Mahasiswa extends Penduduk {
    private String nim;

    Mahasiswa() {
    }

    public Mahasiswa(String dataNim, String dataNama, String dataTempatTanggalLahir) {
        super(dataNama,dataTempatTanggalLahir);
        nim = dataNim;
    }
    
    public void setNim(String dataNim){
        this.nim = nim;
    }
    public String getNim(){
        return nim;
    }
    
    @Override
    public double hitungIuran() {
        double value = Double.parseDouble(nim);
        return value/10000;
    }

}
