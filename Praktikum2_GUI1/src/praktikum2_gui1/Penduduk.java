/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package praktikum2_gui1;

public abstract class Penduduk {
    protected String nama;
    protected String tempatTanggalLahir;
    
    Penduduk(){
        
    }

    public Penduduk(String dataNama, String dataTempatTanggalLahir) {
        nama = dataNama;
        tempatTanggalLahir = dataTempatTanggalLahir;
    }
    
    public void setNama(String dataNama){
        this.nama = nama;
    }
    public String getNama(){
        return nama;
    }
    
    public void setTempatTanggalLahir(String dataTempatTanggalLahir){
        this.tempatTanggalLahir = tempatTanggalLahir;
    }
    public String getTempatTanggalLahir(){
        return tempatTanggalLahir;
    }
    
    public abstract double hitungIuran();
    
    
    
    public static void main(String[] args) {
        // TODO code application logic here
    }
    
}



